call "C:\Program Files\Microsoft Visual Studio\2022\Community\VC\Auxiliary\Build\vcvarsall.bat" arm64 || exit /b 1

cd C:\Users\tcwg\cmake-nightly\cmake

set PATH=%PATH%;C:\Users\tcwg\tools\zulu17.34.19-ca-jdk17.0.3-win_aarch64\bin\

ctest -C Debug -S .\cmake_windows_vs2022_arm64.cmake -V -O test.log -DCMAKE_GENERATOR_PLATFORM=ARM64