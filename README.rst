CMake Dashboard Scripts
***********************

These are CTest scripts used to drive CMake tests for submission to CDash:

* `cmake_windows_vs2022_arm64.cmake`_: Top level script configuring the cmake parameters.
* `cmake_common.cmake`_: Common script to be included by local client scripts.
  See comments at the top of the script for details.
* `kwsys_common.cmake`_: Drive KWSys testing everywhere we test CMake.

.. _`cmake_windows_vs2022_arm64.cmake`: cmake_windows_vs2022_arm64.cmake
.. _`cmake_common.cmake`: cmake_common.cmake
.. _`kwsys_common.cmake`: kwsys_common.cmake
