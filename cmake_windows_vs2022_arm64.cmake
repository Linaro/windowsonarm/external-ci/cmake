set(CTEST_SITE "linaro")
set(CTEST_BUILD_NAME "cmake-windows_vs2022_arm64")
set(CTEST_BUILD_CONFIGURATION Debug)
set(CTEST_CMAKE_GENERATOR "Visual Studio 17 2022")
set(dashboard_root_name "cmake_build")

include(cmake_common.cmake)
